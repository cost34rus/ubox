package com.example.ubox.core

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.ubox.R
import com.example.ubox.domain.model.interfaces.IHasId
import com.example.ubox.domain.model.interfaces.IHasName


abstract class BaseAdapter<T, TBind : ViewDataBinding>(callback: DiffUtil.ItemCallback<T>) :
    RecyclerView.Adapter<BaseViewHolder<ViewDataBinding>>() {

    private val mDiffer: AsyncListDiffer<T?> = AsyncListDiffer(this, callback)

    override fun getItemCount(): Int {
        return mDiffer.currentList.size
    }

    open fun submitList(list: List<T?>?) {
        mDiffer.submitList(list)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ViewDataBinding>, position: Int) {
        (holder as BaseViewHolder<*>).binding.root.setTag(R.string.position, position)
        bind(holder.binding as TBind, position)
    }

    protected open fun getItem(position: Int): T? {
        return mDiffer.currentList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        getViewHolder(parent, viewType)

    open fun getViewHolder(parent: ViewGroup, viewType: Int) =
        BaseViewHolder(createBinding(parent, viewType))

    abstract fun createBinding(parent: ViewGroup, viewType: Int): TBind

    protected abstract fun bind(binding: TBind, position: Int)
}

class DiffByNameCallback<T : IHasName> : DiffUtil.ItemCallback<T>() {

    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean = oldItem == newItem

    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean = oldItem.name == newItem.name

}

class DiffByIdCallback<T : IHasId> : DiffUtil.ItemCallback<T>() {

    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean = oldItem == newItem

    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean = oldItem.id == newItem.id

}
