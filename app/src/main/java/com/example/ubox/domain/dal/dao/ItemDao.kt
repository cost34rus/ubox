package com.example.ubox.domain.dal.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.ubox.domain.model.Item

@Dao
interface ItemDao {

    @Query("SELECT * FROM Item where boxId = :boxId order by id")
    fun getItemsByBoxId(boxId: Long): LiveData<List<Item>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(plants: List<Item>)
}