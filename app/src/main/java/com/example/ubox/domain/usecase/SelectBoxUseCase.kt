package com.example.ubox.domain.usecase

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.example.ubox.domain.model.Item
import com.example.ubox.domain.repo.ItemsRepository

data class SelectBoxDto(
    val items: List<Item>
)

class SelectBoxUseCase(private val itemsRepository: ItemsRepository) {

    fun execute(boxId: Long): LiveData<SelectBoxDto> {
        val items = itemsRepository.getItemsByBoxId(boxId)
        return items.map {
            SelectBoxDto(it)
        }
    }
}