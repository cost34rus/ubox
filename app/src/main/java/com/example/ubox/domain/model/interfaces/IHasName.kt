package com.example.ubox.domain.model.interfaces

interface IHasName {
    val name: String?
}