package com.example.ubox.domain.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.example.ubox.domain.model.interfaces.IHasId
import com.example.ubox.domain.model.interfaces.IHasName

@Entity(
    foreignKeys = [
        ForeignKey(entity = Box::class, parentColumns = ["id"], childColumns = ["boxId"])
    ]
)
data class Item(
    @PrimaryKey(autoGenerate = true) override val id: Long,
    override val name: String?,
    val boxId: Long
) : IHasName, IHasId