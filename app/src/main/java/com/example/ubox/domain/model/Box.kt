package com.example.ubox.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.ubox.domain.model.interfaces.IHasId
import kotlin.random.Random

@Entity
data class Box(
    @PrimaryKey(autoGenerate = true) override val id: Long,
    val name: String
//    var items: List<Item>? = arrayListOf()
) : IHasId {

//    private val _items: List<Item> = listOf(
//        Item("Предмет 0"),
//        Item("Предмет 1"),
//        Item("Предмет 2"),
//        Item("Предмет 4"),
//        Item("Предмет 5"),
//        Item("Предмет 6"),
//        Item("Предмет 7"),
//        Item("Предмет 8"),
//        Item("Предмет 9"),
//        Item("Предмет 10"),
//        Item("Предмет 11")
//    )
//
//    init {
//        items = List(Random.nextInt(5, 40)) {
//            _items[Random.nextInt(0, 11)]
//        }
//    }
}