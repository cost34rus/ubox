package com.example.ubox.ui.main

import androidx.databinding.ObservableField
import com.example.ubox.core.BaseViewModel

class MainActivityViewModel : BaseViewModel(){
    var toolbarTitle: ObservableField<String> = ObservableField()
}