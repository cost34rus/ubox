//package com.example.ubox.domain.usecase
//
//import androidx.lifecycle.LiveData
//import com.example.ubox.domain.model.Box
//import com.example.ubox.domain.repo.BoxRepository
//import com.example.ubox.utils.UseCaseLiveData
//import javax.inject.Inject
//
//class BoxUseCase @Inject internal constructor(
//    private val repository: BoxRepository
//) : UseCaseLiveData<List<Box>, BoxUseCase.BoxParams, BoxRepository>() {
//    class BoxParams() : UseCaseLiveData.Params()
//
//    override fun getRepository(): BoxRepository = repository
//
//    override fun buildUseCaseObservable(params: BoxParams?): LiveData<List<Box>> =
//        repository.getBoxes()
//}