package com.example.ubox.ui.dashboard.boxes.box

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import com.example.ubox.R
import com.example.ubox.core.BaseAdapter
import com.example.ubox.core.DiffByIdCallback
import com.example.ubox.databinding.ItemBoxBinding
import com.example.ubox.domain.model.Box
import com.google.android.material.card.MaterialCardView

class BoxAdapter : BaseAdapter<Box, ItemBoxBinding>(DiffByIdCallback()) {

    var onClick: (Box, ItemBoxBinding) -> Unit = { box, binding -> }
    lateinit var onSelect: (Box) -> Unit
    private val defaultSelectBoxId: Long = 0
    private val selectedBox: MutableLiveData<Box> = MutableLiveData()

    init {
        selectedBox.observeForever {
            onSelect.invoke(it)
        }
    }

    override fun createBinding(parent: ViewGroup, viewType: Int): ItemBoxBinding {
        val binding = ItemBoxBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val viewModel = BoxListItemViewModel()
        binding.viewModel = viewModel
        binding.cardView.setOnClickListener {
            binding.viewModel?.item?.get()?.let {
                selectedBox.postValue(it)
                onClick.invoke(it, binding)
                binding.executePendingBindings()
            }
        }

        return binding
    }

    override fun bind(binding: ItemBoxBinding, position: Int) {
        val item = getItem(position)
        binding.viewModel?.item?.set(item)
        when (position) {
            defaultSelectBoxId.toInt() -> {
                setColorFromSelectBox(binding)
                item?.let { selectedBox.postValue(it) }
            }
            else -> setColorFromUnSelectBoxes(binding)
        }
        selectedBox.observeForever {
            when (it.id) {
                item?.id -> setColorFromSelectBox(binding)
                else -> setColorFromUnSelectBoxes(binding)
            }
            binding.executePendingBindings()
        }
        binding.executePendingBindings()
    }


    @SuppressLint("ResourceType")
    fun setColorFromSelectBox(binding: ItemBoxBinding) {
        binding.viewModel?.backgroundColor?.set(
            Color.parseColor(
                binding.cardView.context.getString(R.color.red)
            )
        )
    }

    @SuppressLint("ResourceType")
    fun setColorFromUnSelectBoxes(binding: ItemBoxBinding) {
        binding.viewModel?.backgroundColor?.set(
            Color.parseColor(
                binding.cardView.context.getString(R.color.yellow)
            )
        )
    }
}

@BindingAdapter("app:cardBackgroundColor")
fun setBackgroundTint(view: MaterialCardView, color: Int) {
    view.setCardBackgroundColor(color)
}