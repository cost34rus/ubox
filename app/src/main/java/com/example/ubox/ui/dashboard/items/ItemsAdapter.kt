package com.example.ubox.ui.dashboard.items

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.ubox.core.BaseAdapter
import com.example.ubox.core.DiffByNameCallback
import com.example.ubox.databinding.ItemItemBinding
import com.example.ubox.domain.model.Item

class ItemsAdapter : BaseAdapter<Item, ItemItemBinding>(DiffByNameCallback()) {

    override fun createBinding(parent: ViewGroup, viewType: Int): ItemItemBinding {
        val binding = ItemItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val viewModel = ItemsListItemViewModel()
        binding.viewModel = viewModel
        return binding
    }

    override fun bind(binding: ItemItemBinding, position: Int) {
        val item = getItem(position)
        binding.viewModel?.item?.set(item)
        binding.executePendingBindings()
    }
}

