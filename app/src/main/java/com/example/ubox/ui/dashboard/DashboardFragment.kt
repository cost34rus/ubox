package com.example.ubox.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ubox.R
import com.example.ubox.databinding.DashboardFragmentBinding
import com.example.ubox.ui.dashboard.boxes.box.BoxAdapter
import com.example.ubox.ui.dashboard.items.ItemsAdapter
import com.example.ubox.utils.LinearLayoutManagerWrapper
import kotlinx.android.synthetic.main.dashboard_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DashboardFragment : Fragment() {

    private val viewModel: DashboardViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DashboardFragmentBinding.inflate(inflater, container, false)
        binding.addItem.setOnClickListener {
            it.findNavController().navigate(R.id.action_dashboardFragment_to_addItemFragment)
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val boxesAdapter = BoxAdapter()
        val itemsAdapter = ItemsAdapter()

        recyclerItem.adapter = itemsAdapter
        recyclerItem.layoutManager =
            LinearLayoutManagerWrapper(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerBox.adapter = boxesAdapter
        recyclerBox.layoutManager =
            LinearLayoutManagerWrapper(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        boxesAdapter.onSelect = { box ->
            textViewName.text = box.name
            viewModel.getSelectedBoxItems(box.id)
                .observe(viewLifecycleOwner) {
                    it.let {
                        itemsAdapter.submitList(it.items)
                    }
                }
        }

        viewModel.boxes.observe(viewLifecycleOwner) {
            boxesAdapter.submitList(it)
        }
    }
}