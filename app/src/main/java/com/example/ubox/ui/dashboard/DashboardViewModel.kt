package com.example.ubox.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.ubox.domain.model.Box
import com.example.ubox.domain.repo.BoxRepository
import com.example.ubox.domain.usecase.SelectBoxDto
import com.example.ubox.domain.usecase.SelectBoxUseCase

class DashboardViewModel(
    boxRepository: BoxRepository,
    private val selectBoxUseCase: SelectBoxUseCase
) : ViewModel() {

    val boxes: MutableLiveData<List<Box>> = MutableLiveData()

    init {
        boxRepository.getBoxes().observeForever {
            boxes.postValue(it)
        }
    }

    fun getSelectedBoxItems(selectBoxId: Long): LiveData<SelectBoxDto> = selectBoxUseCase
        .execute(selectBoxId)
}