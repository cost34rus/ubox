package com.example.ubox.domain.repo

import androidx.lifecycle.LiveData
import com.example.ubox.domain.dal.dao.ItemDao
import com.example.ubox.domain.model.Item

class ItemsRepository(private val itemDao: ItemDao) {

    fun getItemsByBoxId(boxId: Long): LiveData<List<Item>> = itemDao.getItemsByBoxId(boxId)
}