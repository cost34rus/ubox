package com.example.ubox.domain.repo

import androidx.lifecycle.LiveData
import com.example.ubox.domain.dal.dao.BoxDao
import com.example.ubox.domain.model.Box

class BoxRepository(private val boxDao: BoxDao) {

    fun getBoxes(): LiveData<List<Box>> = boxDao.getBoxes()
}