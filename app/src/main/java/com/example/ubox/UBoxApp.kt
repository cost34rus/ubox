package com.example.ubox

import android.app.Application
import com.example.ubox.domain.dal.AppDatabase
import com.example.ubox.domain.repo.BoxRepository
import com.example.ubox.domain.repo.ItemsRepository
import com.example.ubox.domain.usecase.SelectBoxUseCase
import com.example.ubox.ui.dashboard.DashboardFragment
import com.example.ubox.ui.dashboard.DashboardViewModel
import com.example.ubox.ui.dashboard.boxes.box.BoxListItemViewModel
import com.example.ubox.ui.dashboard.items.ItemsListItemViewModel
import com.example.ubox.ui.main.MainActivityViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module

class UBoxApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@UBoxApp)
            modules(appModule, fragmentModule, viewModelModule)
        }
    }
}

val appModule = module {
    single { AppDatabase.getInstance(androidContext()) }
    //repo
    single { BoxRepository(get()) }
    single { ItemsRepository(get()) }
    //useCases
    single { SelectBoxUseCase(get()) }
    //dao
    single { get<AppDatabase>().boxDao() }
    single { get<AppDatabase>().itemDao() }
}

val viewModelModule = module {
    factory { DashboardViewModel(get(), get()) }
    factory { MainActivityViewModel() }
    factory { BoxListItemViewModel() }
    factory { ItemsListItemViewModel() }
}

val fragmentModule = module {
    factory { DashboardFragment() }
}