package com.example.ubox.workers

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.ubox.domain.dal.AppDatabase
import com.example.ubox.domain.model.Box
import com.example.ubox.domain.model.Item
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import kotlinx.coroutines.coroutineScope

const val BOX_DATA_FILENAME = "box_data.json"
const val ITEM_DATA_FILENAME = "item_data.json"

class SeedDatabaseWorker(
    context: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(context, workerParams) {

    companion object {
        private val TAG = SeedDatabaseWorker::class.java.simpleName
    }

    override suspend fun doWork(): Result = coroutineScope {
        try {
            applicationContext.assets.open(BOX_DATA_FILENAME).use { inputStream ->
                JsonReader(inputStream.reader()).use { jsonReader ->
                    val type = object : TypeToken<List<Box>>() {}.type
                    val list: List<Box> = Gson().fromJson(jsonReader, type)
                    val appDatabase: AppDatabase = AppDatabase.getInstance(applicationContext)
                    appDatabase.boxDao().insertAll(list)

                    Result.success()
                }
            }
            applicationContext.assets.open(ITEM_DATA_FILENAME).use { inputStream ->
                JsonReader(inputStream.reader()).use { jsonReader ->
                    val type = object : TypeToken<List<Item>>() {}.type
                    val list: List<Item> = Gson().fromJson(jsonReader, type)
                    val appDatabase: AppDatabase = AppDatabase.getInstance(applicationContext)
                    appDatabase.itemDao().insertAll(list)

                    Result.success()
                }
            }
        } catch (ex: Exception) {
            Log.e(TAG, "Error seeding database", ex)
            Result.failure()
        }
    }
}