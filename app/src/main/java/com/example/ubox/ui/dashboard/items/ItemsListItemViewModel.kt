package com.example.ubox.ui.dashboard.items

import androidx.databinding.ObservableField
import com.example.ubox.domain.model.Item

class ItemsListItemViewModel {
    var item = ObservableField<Item>()
}