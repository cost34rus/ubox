package com.example.ubox.domain.dal.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.ubox.domain.model.Box

@Dao
interface BoxDao {

    @Query("SELECT * FROM Box ORDER BY id")
    fun getBoxes(): LiveData<List<Box>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(plants: List<Box>)
}