package com.example.ubox.domain.model.interfaces

interface IHasId {
    val id: Long
}