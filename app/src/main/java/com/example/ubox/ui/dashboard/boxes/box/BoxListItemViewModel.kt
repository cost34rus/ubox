package com.example.ubox.ui.dashboard.boxes.box

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.ubox.domain.model.Box

class BoxListItemViewModel: ViewModel() {
    var item = ObservableField<Box>()
    var backgroundColor = ObservableField<Int>()
}