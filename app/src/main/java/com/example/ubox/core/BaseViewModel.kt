package com.example.ubox.core

import androidx.lifecycle.ViewModel

/**
 * Created by Furkan on 2019-10-16
 */

open class BaseViewModel : ViewModel()
